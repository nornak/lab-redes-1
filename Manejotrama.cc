
#include "Manejotrama.h" 

Trama * crearTrama(const char *msj, int tipo)
{
    Trama * nuevo = new Trama(msj, tipo);
    nuevo->setTipo(tipo);
    return nuevo;
}




Trama * crearMensaje(const char *msj, int tipo, const char *dato)
{
    Trama * nuevo = crearTrama(msj, tipo);
    nuevo->setDato(dato);
    nuevo->setDireccion(0);
    nuevo->setTipo(0);
    nuevo->setMsj(0);
    nuevo->setBitPF(0);
    nuevo->setNS(0);
    nuevo->setNR(0);
    nuevo->setOrigen(-1);
    nuevo->setNumero(0);

    return nuevo;
}



Trama * crearTramaI(int NS, int NR)
{
    string nombre = "I,";
    nombre += convertInt(NS);
    nombre += ",";
    nombre += convertInt(NR);
    Trama * nuevo = crearTrama(nombre.c_str(), TRAMA_I);
    nuevo->setNS(NS);
    nuevo->setNR(NR);
    nuevo->setOrigen(-1);
    nuevo->setNumero(0);
    return nuevo;
}




Trama * crearTramaI(int NS)
{
    string nombre = "I,";
    nombre += convertInt(NS);
    nombre += ",";
    nombre += "0";
    Trama * nuevo = crearTrama(nombre.c_str(), TRAMA_I);
    nuevo->setNS(NS);
    nuevo->setOrigen(-1);
    nuevo->setNumero(0);
    return nuevo;
}




Trama * crearTramaS(const char* msj, int NR, tipo_s modo)
{
    string nombre(msj);
    nombre += " ";
    nombre += convertInt(NR);
    Trama * nuevo = crearTrama(nombre.c_str(), TRAMA_S);
    nuevo->setNS(-1);
    nuevo->setNR(NR);
    nuevo->setBitPF(0);
    nuevo->setMsj(modo);
    nuevo->setOrigen(-1);
    nuevo->setNumero(0);
    return nuevo;
}



Trama * crearTramaS(const char* msj, int NR, tipo_s modo, bitPF bit )
{
    string nombre(msj);
    nombre += " ";
    nombre += convertInt(NR);

    if(bit == B_P)
    {
        nombre += " P";
    }
    else if(bit == B_F)
    {
        nombre += " F";
    }

    Trama * nuevo = crearTrama(nombre.c_str(), TRAMA_S);
    nuevo->setNS(-1);
    nuevo->setNR(NR);
    nuevo->setBitPF(bit);
    nuevo->setMsj(modo);
    nuevo->setOrigen(-1);
    nuevo->setNumero(0);
    return nuevo;
}



Trama * crearTramaS(const char* msj, int NR, tipo_s modo, bitPF bit, int dir )
{
    string nombre(msj);
    nombre += " ";
    nombre += convertInt(NR);

    if(bit == B_P)
    {
        nombre += " P";
    }
    else if(bit == B_F)
    {
        nombre += " F";
    }

    Trama * nuevo = crearTrama(nombre.c_str(), TRAMA_S);
    nuevo->setNS(-1);
    nuevo->setNR(NR);
    nuevo->setBitPF(bit);
    nuevo->setMsj(modo);
    nuevo->setOrigen(dir);
    nuevo->setNumero(0);
    return nuevo;
}

string convertInt(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}




Trama * iniciarComunicacion(modoConexion tipo)
{
    Trama *sincro = NULL;

    if( tipo == NRM)
    {
        sincro = new Trama("SNRM P", TRAMA_U);
        sincro->setMsj(SNRM);
        sincro->setTipo(TRAMA_U);
        // bit P
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }
    else if (tipo == NRME)
    {
        sincro = new Trama("SNRME P", TRAMA_U);
        sincro->setMsj(SNRME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }
    else if (tipo == ABM)
    {
        sincro = new Trama("SABM P", TRAMA_U);
        sincro->setMsj(SABM);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }
    else if (tipo == ABME)
    {
        sincro = new Trama("ABME P", TRAMA_U);
        sincro->setMsj(SABME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }
    else if (tipo == ARM)
    {
        sincro = new Trama("ARM P", TRAMA_U);
        sincro->setMsj(SARM);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }
    else if (tipo == ARME)
    {
        sincro = new Trama("ARME P", TRAMA_U);
        sincro->setMsj(SARME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
        sincro->setNumero(0);
    }

    return sincro;
}


modoConexion obtenerModo(Trama *trama)
{
    int msj = trama->getMsj();

    if (msj == SNRM)
    {
        return NRM;
    }
    else if (msj == SNRME)
    {
        return NRME;
    }
    else if (msj == SARM)
    {
        return ARM;
    }
    else if (msj == SARME)
    {
        return ARME;
    }
    else if (msj == SABM)
    {
        return ABM;
    }
    else if (msj == SABME)
    {
        return ABME;
    }
    // cuando la trama no es de seteo de conexion
    // es para diferenciar del resto
    return S_C;
}

void desencapsular(Trama *trama)
{
    trama->setDireccion(0);
    trama->setTipo(0);
    trama->setMsj(0);
    trama->setBitPF(0);
    trama->setNS(0);
    trama->setNR(0);
}




Trama *copiarTrama(Trama *t)
{
    Trama *nueva = crearTrama(t->getName(), t->getTipo());
    nueva->setDireccion(t->getDireccion());
    nueva->setTipo(t->getTipo());
    nueva->setMsj(t->getMsj());
    nueva->setBitPF(t->getBitPF());
    nueva->setNS(t->getNS());
    nueva->setNR(t->getNR());
    nueva->setDato(t->getDato());
    nueva->setNumero(t->getNumero());
    return nueva;
}
