#ifndef _MANEJO_TRAMA_
#define _MANEJO_TRAMA_

#include <iostream>
#include <sstream>
#include <string>

#include <omnetpp.h>
#include "Trama_m.h"

using namespace std;

// los modos de la trama del tipo no-numerada
enum tipo_u
{
    // modos de conexion
    SNRM = 0,
    SNRME,
    SARM,
    SARME,
    SABM,
    SABME,

    UI,    // unnumbered information:  informacion no-numerada    -> P/F
    UA,    // unnumbered acknowledged: confirmacion no-numerada   -> F
    DISC,  // solicitud de desconexion, se responde con UA        -> P
    DM,    // disconnect mode: respuesta a DISC                   -> F
    RD,    // request disconnect: solicitud de desconexion (DISC) -> F
    SIM,   // set initialization mode: Initialize link control function in the addressed station    -> P
    RIM,   // Request Initialization Mode: inicializacion necesaria, solicitude de SIM              -> F
    UP,    // Unnumbered Poll: se utiliza para solicitar informacion de control                     -> P
    RSET,  // reset: sirve para reiniciar la conexion, se pone a cero los contadores y las ventanas deslizantes
    XID,   // Exchange Identification: se utiliza para el intercambio de capacidades de los terminales  -> P/F
    TEST,  // test: is simply a ping command for debugging purposes                                     -> P/F                                      
    FRMR,  // Frame Reject: Reporte de la recepción de marco inaceptable                                -> F
    NR0,   // Nonreserved 0: no estanderizado
    NR1,   // Nonreserved 1: no estanderizado
    NR2,   // Nonreserved 2: no estanderizado
    NR3    // Nonreserved 3: no estanderizado
};

enum modoConexion
{
    S_C,    // sin conexion
    NRM,    // modo de respuesta normal
    NRME,   // modo de respuesta normal extendido
    ABM,    // modo balanceado Asincrono
    ABME,   // modo balanceado Asincrono extendido
    ARM,    // modo de respuesta Asincrono
    ARME    // modo de respuesta Asincrono extendido
};

// los modos de la trama supervisora
enum tipo_s
{
    RR = 0, // si la primaria envia con P, indica que esta pidienfo informacion de la segunda estacion
            // si esta el bit F, es una respuesta de la segunda estacion hacia la primera
    REJ,
    RNR,
    SREJ
};

enum bitPF 
{
    B_NO,
    B_P,
    B_F
};

using namespace std;

/***********
 * retorna el puntero de una trama del tipo 'tipo', el cual mostrara por la
 * interfaz 'msj'
 *
 * @param msj - el mensaje que se muestra en la interfaz.
 * @param tipo - indica el tipo de trama que es, puede ser: TRAMA_I, TRAMA_S,
 * TRAMA_U.
 *
 * @return - un puntero a una nueva trama.
 */
Trama * crearTrama(const char *msj, int tipo);




/***********
 * retorna el puntero de una trama del tipo 'tipo', el cual mostrara por la
 * interfaz 'msj' y que contiene el dato 'dato'.
 *
 * @param msj - el mensaje que se muestra en la interfaz.
 * @param tipo - indica el tipo de trama que es, puede ser: TRAMA_I, TRAMA_S,
 * TRAMA_U.
 * @param dato - el mensaje que se enviara por la trama
 *
 * @return - un puntero a una nueva trama.
 */
Trama * crearMensaje(const char *msj, int tipo, const char *dato);




/***********
 * Retorna un puntero a una trama del tipo Trama_I.
 * Muestra un mensaje de este estilo: 'I,NS,NR'
 * @param NS - el numero del paquete enviado
 * @param NR - numero de paquetes recibidos
 *
 * @return - puntero a la nueva trama creada
 */
Trama * crearTramaI(int NS, int NR);




/***********
 * Retorna un puntero a una trama del tipo Trama_I.
 * Muestra un mensaje de este estilo: 'I,NS,0'
 *
 * @param NS - el numero del paquete enviado
 *
 * @return - puntero a la nueva trama creada
 */
Trama * crearTramaI(int NS);




/***********
 * Retorna el puntero a una nueva Trama del tipo TRAMA_S, contiene el mensaje
 * que se muestra en la interfaz, ademas contiene el numero del paquete que
 * espera. Tambien hay que indicarle el tipo de trama_s, si es Receive Ready (RR),
 * Recieve not Ready (RNR), Reject (REJ) o Selective Reject (SREJ). 
 * Agrega el valor de NR al mensaje que se muestra graficamente
 *
 * @param msj - el mensaje que se muestra en la interfaz.
 * @param NR - el numero del paquete que espera recibir, con NR - 1 paquetes confirmados
 * @param modo - modo de trama supervisora, puede tomar los valores: RR, RNR,
 * REJ, SREJ.
 *
 * @return - un puntero a la trama creada
 */
Trama * crearTramaS(const char* msj, int NR, tipo_s modo);


/***********
 * Retorna el puntero a una nueva Trama del tipo TRAMA_S, contiene el mensaje
 * que se muestra en la interfaz, ademas contiene el numero del paquete que
 * espera. Tambien hay que indicarle el tipo de trama_s, si es Receive Ready (RR),
 * Recieve not Ready (RNR), Reject (REJ) o Selective Reject (SREJ). 
 * Agrega el valor de NR al mensaje que se muestra graficamente
 *
 * @param msj - el mensaje que se muestra en la interfaz.
 * @param NR - el numero del paquete que espera recibir, con NR - 1 paquetes confirmados
 * @param modo - modo de trama supervisora, puede tomar los valores: RR, RNR,
 * REJ, SREJ.
 * @param bit - indica la presencia o ausencia del bit P/F 
 *
 * @return - un puntero a la trama creada
 */
Trama * crearTramaS(const char* msj, int NR, tipo_s modo, bitPF bit);


Trama * crearTramaS(const char* msj, int NR, tipo_s modo, bitPF bit, int dir);

/***********
 * Crear una trama del tipo no-numerada indicando el tipo de conexion ingresada
 * como parametro
 * @param tipo - indica el tipo de conexion que se quiere establecer.
 * @return - puntero a la trama creada.
 */
Trama * iniciarComunicacion(modoConexion tipo);




/***********
 * obtiene el tipo de conexion que solicita la trama
 * Es para cuando se recibe una trama tipo TRAMA_U.
 * @param trama - trama a verificar
 * @return modoConexion - uno de los valores definidos para este tipo de variable
 */
modoConexion obtenerModo(Trama *trama);


/***********
 *"Borra todo lo que no tiene que ver con el mensaje de la aplicacion"
 *Es para cuando recibe una trama tipo TRAMA_I.
 *@param trama - trama a tratar
 */
void desencapsular(Trama *trama);

// envia un mensaje de finalizacion de la comunicacion
void finalizarComunicacion();

// envia un mensaje 
void enviarMensaje();

string convertInt(int n);

/***********
 * Copia el contenido de una trama
 * @param t - trama a copiar
 * @return - puntero a la nueva trama
 */
Trama *copiarTrama(Trama *t);


#endif
