/* Envía las palabras desde la capa intermedia al otro host
 * y recibe las palabras que llegan desde el otro host y las envía al nivel
 * superior
*/

#include <string>
#include <omnetpp.h>
#include "Manejotrama.h"
#include <vector>

using namespace std;




class Enlace : public cSimpleModule
{
    private:
        // prob de error en los mensajes
        double error;
        // tramas a enviar
        int n_trama;
        // tramas que envia el otro host
        int n_trama_otro;
        // cada cuanto enviar un ack
        int n_ack;
        // cantidad recibida
        int NR;
        // cantidad enviada
        int NS;
        //tamaño de la ventana
        int tam_ventana;
        // direccion del host
        int direccion;
        //indice maximo de la ventana
        int max_indice;
        //valor que indica si se puede enviar dato al otro host
        bool listoS;
        // para indicar si se produce un rechazo simple
        bool estadoREJ;
        //contador de dato recibido
        int cont_ack;
        //contador para enumerar las tramas
        int contador;
        int n_trama_max;
        int n_trama_otro_max;
        
        vector <Trama*> bufferTramas;
        vector <Trama*> bufferSM;

        simtime_t timeout;
        simtime_t intervalo_transmision;
        
        // tiene valor S_C, cuando no hay una conexion establecida
        modoConexion modo_conexion;

        modoConexion conexion_pedida;
        
        // guardar la trama que se envia para el timeout
        // para despues cancelarla, con cancelEvent
        Trama * tramaTimeOutI;
        Trama * tramaTimeOutS;
        Trama * tramaTimeOutU;
        

    protected:

        virtual void initialize();
        
        //Toma la decision de que manejador llamar
        virtual void handleMessage(cMessage *msg);
        
        // procesa el mensaje si viene de arriba (intermedio)
        virtual void desdeArriba(Trama *msg);
        //Procesador de mensaje desde Intermedio
        virtual void procesarMsgDelIntermedio(Trama *dato);
        
        // procesa el mensaje si viene desde abajo (fisico)
        virtual void desdeAbajo(Trama *msg);
        //Procesador de TramaI que proviene del otro host
        virtual void procesarTramaDeInformacion(cMessage *packet);
        //Procesador de TramaS que viene del otro host
        virtual void procesarTramaSupervisora(cMessage *packet);
        //Procesador de TramaN del otro host
        virtual void procesarTramaNoNumerada(cMessage *packet);

        // Envia la trama al otro host, comprobando ventana, tramas,etc. tambien
        // creaa el mensaje en caso de error de transmicion
        virtual void enviarTramaInfo(Trama *msj);

        // envia una trama_S al otro lado, si esa presente el bit P, se crea un
        // mensaje en caso de que fallase el envio del mensaje
        virtual void enviarTramaS(const char *msj, int NR, tipo_s modo, bitPF bit);

        // cuando ocurre un error en la retransmicion, se llama a esta funcion.
        // Se activa cuando se pasa el timeout y reenvia el mensaje correspondiente
        virtual void retransmicion(Trama *msj);

        virtual bool puedeEnviar();

        virtual void establecerComunicacion(Trama *msj);

        virtual void cancelarEventos(vector<Trama*> buffer);
};

int estado = 0;

//Definición de los métodos
Define_Module( Enlace );



void Enlace::initialize()
{
    error = par("error");
    n_trama = par("n_trama");
    n_trama_otro = par("n_trama_otro");
    n_ack = par("n_ack");
    tam_ventana = par("tam_ventana");
    direccion = par("direccion");
    
    ev << "error: " << error << endl;
    ev << "n_trama: " << n_trama << endl;
    ev << "n_trama_otro: " << n_trama_otro << endl;
    ev << "n_ack: " << n_ack << endl;
    
    n_trama_max = n_trama;
    n_trama_otro_max = n_trama_otro;

    modo_conexion = conexion_pedida = S_C;
    
    max_indice = tam_ventana;

    cont_ack = 0;

    contador = 0;
    
    estadoREJ = false;

    NS = 0;
    NR = 0;
    listoS = true;

    timeout = 10.0;
    intervalo_transmision = 0.07;

    tramaTimeOutI = tramaTimeOutS = tramaTimeOutU = NULL;
    // para ver el valor al hacer click en enlace en la interfaz
    WATCH(NR);
    WATCH(NS);
}


// Manejador de mensajes
void Enlace::handleMessage(cMessage *msg)
{
    ev << "En Host: " << direccion << endl;
    ev << "la n_trama: " << n_trama << " tam_ventana: " << tam_ventana <<  endl;
    
    Trama *msj = check_and_cast<Trama *>(msg);

    // si llega desde arriba no se toma en cuenta los errores de transmicion
    if( msj->arrivedOn("recibir_arriba"))
    {
        ev << "desde intermedio" << endl;
        // del intermedio
        desdeArriba(msj);
    }
    // cuando recibe una trama programada (por scheduleAt)
    else if( msj->isSelfMessage() )
    {
        EV << "timeout ->" << endl;

        retransmicion(msj);

        delete msj;

    }else{

        //Si el mensaje llega desde el otro Host
        if (msg->arrivedOn("recibir_fisico"))
        {
            ev << "desde fisico" << endl;
            // del otro host
            desdeAbajo(msj);
        }
    }
}

// maneja los mensajes que llegan desde arriba, del intermedio
void Enlace::desdeArriba(Trama *msj)
{
    // si no hay conexion, se crea
    if(modo_conexion == S_C and estado == 0)
    {
        estado = 1;
        bubble("estableciendo conexion");
        EV << "sin conexion -> estableciendo" << endl;

        establecerComunicacion(msj);

    }
    // cuando ya hay conexion, se envia el paquete
    else{
        procesarMsgDelIntermedio(msj);
    }
}

// maneja los mensajes que llegan desde abajo, del fisico
void Enlace::desdeAbajo(Trama *msj)
{
    switch(msj->getTipo())
    {
        case TRAMA_I:
            if (uniform(0,1) < error)
            {
                EV << "mensaje perdido " << endl;
                bubble("mensaje perdido");  
            }else{
                procesarTramaDeInformacion(msj);
            }

            break;
            
        case TRAMA_S:
            procesarTramaSupervisora(msj);
            break;

        case TRAMA_U:
            procesarTramaNoNumerada(msj);
            break;
    }
}

void Enlace::retransmicion(Trama *msj)
{
    EV << "retransmicion ";

    switch(msj->getTipo())
    {
        case TRAMA_I:
            EV << "-> TRAMA_I" << endl;

            enviarTramaInfo(crearTramaI(NS));

            cancelEvent(bufferSM.front());
            bufferSM.erase(bufferSM.begin());
            
            break;

        case TRAMA_S:
            EV << "-> TRAMA_S" << endl;
            
            

            ev << endl;
            ev << "tamaño del buffer es "<< bufferTramas.size()<<endl;
            ev << endl;
            
            cancelEvent(bufferTramas.front());
            bufferTramas.erase(bufferTramas.begin());

            
            ev << endl;
            ev << "tamaño del buffer es "<< bufferTramas.size()<<endl;
            ev << endl;
            
            enviarTramaS("RR", NR, RR, B_P);
            
            break;

        case TRAMA_U:
            EV << "-> TRAMA_U" << endl;

            Trama *trama = iniciarComunicacion(obtenerModo(msj));

            send(trama, "enviar_fisico");

            //tramaTimeOutU = copiarTrama(trama);

            //scheduleAt(simTime() + timeout, tramaTimeOutU);
        
            break;
    }
}


// se hace el procesamiento del mensaque que llega del nivel intermedio
// intermedio -> fisico
void Enlace::procesarMsgDelIntermedio(Trama *trama)
{
    
    if(puedeEnviar())
    {
        EV << "puede enviar " << endl;

        Trama *temp = crearTramaI(NS, NR);
 
        enviarTramaInfo(temp);

        delete trama;

    }else{
        delete trama;
    }
}


// procesa el paquete cuando viene del otro lado
void Enlace::procesarTramaDeInformacion(cMessage *packet)
{
    ev << "procesando TRAMA_I" << endl;
    
    Trama *msj = check_and_cast<Trama *>(packet);


    //Enviar el paquete hacia el nivel intermedio
    //msj->setName("InformacionRecibido");
    //send(msj, "enviar_arriba");
    
    EV << "NR: " << NR << " getNS " << msj->getNS() << endl;
    
    // si son distintos, hay un problema
    if( msj->getNS() != NR and estadoREJ == false)
    {
        estadoREJ = true;
        EV << "son distintos !!! -> REJ " << NR << endl;
        enviarTramaS("REJ", NR, REJ, B_NO);
        delete msj;
        estadoREJ = false;
    }
    else if(msj->getNS() != NR and estadoREJ == true)
    {
        EV << "descartando" << endl;
        delete msj;

    }
    else{

        NR++;
        
        if(NR == max_indice)
        {
            NR = 0; 
        }
        
        cont_ack++;

        if (cont_ack == n_ack)
        {
            // envia el ACK al otro host
            enviarTramaS("RR", NR, RR, B_NO);
            cont_ack = 0;
        }
    }
}



// se recibe una trama del tipo supervisora del otro host
void Enlace::procesarTramaSupervisora(cMessage *packet)
{
    Trama *msj = check_and_cast<Trama *>(packet);

    ev << "procesando TRAMA_S" << "->";

    // mensaje con bit P
    if(msj->getBitPF() == B_P)
    {
        EV << "con bit P"; 

        if(msj->getMsj() == RR)
        {
            EV << " tipo RR" << endl;

            
            enviarTramaS("RR", NR, RR, B_F);
        }
    }
    // mensajes con bit F
    else if(msj->getBitPF() == B_F)
    {
        EV << "con bit F"; 

        if(msj->getMsj() == RR)
        {
            EV << " tipo RR" << endl;

            EV << "RR F quitando: " << tramaTimeOutS << endl;

            cancelarEventos(bufferTramas);
            bufferTramas.clear();

            ev << endl;
            ev << "tamaño del buffer es "<< bufferTramas.size()<<endl;
            ev << endl;
            
            NS = msj->getNR();

            tam_ventana += contador;
            n_trama += contador;
            
            contador = 0;

            if(tam_ventana >  max_indice){
                tam_ventana = max_indice;
            }

            if(n_trama > n_trama_max){
                n_trama = n_trama_max;
            }

            EV << "la n_trama: " << n_trama << " tam_ventana: " << tam_ventana <<  endl;

            enviarTramaInfo(crearTramaI(NS, NR));

        }
    }
    // si recibe un RR, confirmacion de recepcion
    else if(msj->getMsj() == RR and msj->getBitPF() == B_NO)
    {
        EV << " llego RR" << endl;

        tam_ventana += n_ack;
        
        contador -= n_ack;

        listoS = true;
        
        if(tam_ventana >  max_indice){
            tam_ventana = max_indice;
        }

        EV << "RR confirmacion cancelando: " << bufferTramas.front() << endl;
        //cancelEvent(tramaTimeOutS);
        
        int i;
        i = n_ack;
        
        while(i > 0){
            cancelEvent(bufferTramas.front());
            bufferTramas.erase(bufferTramas.begin());
            i--;
        }
    }
    
    else if(msj->getMsj() == REJ)
    {
        EV << "llego REJ" << endl;

        tam_ventana += contador;
        n_trama += contador;
        
        contador = 0;

        cancelarEventos(bufferTramas);
        bufferTramas.clear();
        cancelarEventos(bufferSM);
        bufferSM.clear();

        if(tam_ventana >  max_indice){
            tam_ventana = max_indice;
        }

        if(n_trama > n_trama_max){
            n_trama = n_trama_max;
        }
        
        EV << "la n_trama: " << n_trama << " tam_ventana: " << tam_ventana <<  endl;
        
        NS = msj->getNR();
        enviarTramaInfo(crearTramaI(msj->getNR(), NR));

    }

    else if(msj->getMsj() == RNR)
    { 
        ev << "llego RNR " << NR <<endl;
        
        //NR = msj->getNS();
        
        tam_ventana += n_ack;
        
        contador -= n_ack;
        
        listoS = false;
        
        if(tam_ventana >  max_indice){
            tam_ventana = max_indice;
        }
    

    }
    delete msj;
}




void Enlace::procesarTramaNoNumerada(cMessage *packet)
{
    ev << "procesando TRAMA_U -> " ;

    Trama *msj = check_and_cast<Trama *>(packet);

    Trama *respuesta;

    // si no hay conexion y si hay una peticion, se envia una confirmacion
    if ( modo_conexion == S_C and ((modo_conexion = obtenerModo(msj)) != S_C) )
    {
        EV << "enviando respuesta ->";

        EV << "conexion: " << modo_conexion << endl;

        // se puede responder afirmativo o rechazar la conexion
        // Afirmativo -> enviar UA
        // Negativo -> enviar DM

        respuesta = crearTrama("UA F", TRAMA_U);

        respuesta->setMsj(UA);
        // tiene bit F
        respuesta->setBitPF(2);

        modo_conexion = conexion_pedida;
        
        send(respuesta, "enviar_fisico");
        delete msj;
    }
    // Recibe la confirmacion, continua enviando datos
    else if (msj->getMsj() == UA)
    {
        bubble("conexion confirmada");

        EV << "conexion confirmada" << endl;

        //cancelEvent(tramaTimeOutU);

        EV << "algo UA confirm -> TO " << tramaTimeOutU << " | "<< msj << endl;

        modo_conexion = conexion_pedida;

        procesarMsgDelIntermedio(crearTramaI(NS, NR));

        delete msj;
    }

    // TODO poner condiciones para otros tipos de valores de tramas no numeradas
}




void Enlace::enviarTramaInfo(Trama *msj)
{   
    if( n_trama == 0)
    {
        //TODO DESCONECTARSE 
        endSimulation();
    }

    if( puedeEnviar() )
    {
        
        tam_ventana--;
        n_trama--;
 
        contador++;

        EV <<  "Enviando TRAMA_I ->\nn_trama:  " << n_trama << " tam_ventana " << tam_ventana << endl;
        
        send(msj, "enviar_fisico");
        
        NS++; 
        
        if(NS == max_indice)
        { 
            NS = 0;
        }
        
        // time out
        tramaTimeOutS = crearTramaS( "RR", 0, RR, B_P, direccion );
        
        Trama *tramatimeout = copiarTrama(tramaTimeOutS);

        bufferTramas.push_back(tramatimeout);

        string nombre = "SM S, 0," + convertInt(0) ;
        tramatimeout->setName(nombre.c_str());

        // schedule para hacer la transmicion
        scheduleAt(simTime() + timeout, tramatimeout);

        EV << "selfM creado -> RR" << tramatimeout << endl;

        // schedule para enviar otra trama de info
        Trama *aux = crearTramaI(NS, NR);
        nombre = "SM I," + convertInt(NS) + "," + convertInt(NR) ;
        aux->setName(nombre.c_str());

        bufferSM.push_back(aux);
        scheduleAt(simTime() + intervalo_transmision, aux);
    }
}


void Enlace::enviarTramaS(const char *msj, int NR, tipo_s modo, bitPF bit)
{
    Trama *rr = crearTramaS(msj, NR, modo, bit, direccion);

    send(rr, "enviar_fisico"); 

    // el bit F no requiere confirmacion asi que no es necesario un msj cuando se pierde
    if (bit == B_P)
    {
        EV << "en if: " << bit << " ->" ; 

        tramaTimeOutS = crearTramaS( "RR", NR, RR, bit, direccion);

        //scheduleAt(simTime() + timeout, tramaTimeOutS);

        //EV << "selfM creado -> RR" << tramaTimeOutS << endl;
    }
}

bool Enlace::puedeEnviar()
{
    if(tam_ventana > 0 and listoS and modo_conexion != S_C and n_trama > 0)
    {
        return true;
    }else{
        EV << "no puede enviar" << endl;
        return false;
    }
}

void Enlace::establecerComunicacion(Trama *msj)
{
    Trama *set = iniciarComunicacion(NRM);

    conexion_pedida = NRM;

    // guarda el mensaje que provenia desde aplicacion

    tramaTimeOutU = copiarTrama(set);

    send(set, "enviar_fisico");

    // mensaje time out
    /*scheduleAt(simTime() + timeout, tramaTimeOutU);
    EV << "selfM creado-> S_C" << tramaTimeOutU << endl;*/

}

void Enlace::cancelarEventos(vector<Trama*> buffer)
{
    unsigned n;

    n = buffer.size();

    for(unsigned i=0; i < n; i++){
        cancelEvent(buffer.front());
        buffer.erase(buffer.begin());
    }
}
