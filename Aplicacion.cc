/*
 * En éste módulo se generan palabras de información, la cual viaja
 * a los modulos inferiores para ser enviada al otro Host a través del canal
 */
#include <string.h>
//librería de OMNeT++
#include <omnetpp.h>
#include <cstdlib>
#include <iostream>
#include "Trama_m.h"
#include "Manejotrama.h"

using namespace std;

//Nombre de la clase y tipo de la librería omnetpp.h
class Aplicacion : public cSimpleModule
{
    private: 
        int n_trama;

    public:
        //Inicializar módulo
        virtual void initialize();
        //Manejador de mensajes
        virtual void handleMessage(cMessage *msg);
        //Generador de palabras
        virtual void generaPalabraInfo();
};


//Definición de las funciones de aplicación
Define_Module(Aplicacion);



//Inicializar
void Aplicacion::initialize()
{
    n_trama = par("n_trama");

    if(n_trama > 0){
        //Generar palabra para envío
        generaPalabraInfo();
    }
}



//Manejar mensajes
void Aplicacion::handleMessage(cMessage *msg)
{
    //Si el mensaje ha llegado
    if (msg->arrivedOn("recibir_abajo"))
    {
        //Borrar el mensaje
        delete msg;//cuando llega un mensaje solo se descarta
    }
}



//Generador de palabras
void Aplicacion::generaPalabraInfo()
{
    if( n_trama > 0)
    {
        Trama *msj = crearMensaje("InformacionEnviado", TRAMA_I, "Datos");
        send(msj, "enviar_abajo");
    }
}
