
# Laboratorio de Redes de Comunicación

Utilizando oMNeT++ para simular la red que tenemos que crear
Consiste en utilizar el protocolo HDLC.

# descripción

- Realizar un esquema de dos maquinas, con 3 niveles, emulando el estándar OSI
- Capa de aplicación, intermedia y de enlace
- Utilizando el protocolo HDLC
- Decidir la mejor implementación para el problema

## Pasos para la comunicacion

- Se inicia la simulación
- Operaciones necesarias para establecer la conexión de acuerdo a HDLC
- Si hay conexión, setear las ventanas deslizantes del emisor y receptor
- se envían las X tramas a la otra estación
- se recibe y se envían tramas de asentimiento, de acuerdo al bit P/F
- Cuando todas las tramas son asentidas se cierra la conexión
- Diseñar la simulación para los siguientes casos:
- Solo A transmite, B únicamente asiente
- Solo B transmite, A únicamente asiente
- Ambas estaciones transmiten, ambas asienten, utilizando piggybacking cuando es posible


## Detalles a tener en cuenta

- Implementación consistente con la teoría de HDLC
- Tomar el formato de la trama de HDLC
- La solicitud de las tramas supervisoras se hace con el bit P/F
- Simular la ocurrencia de errores en las tramas dada una determinada probabilidad
- Las tramas se generan en el nivel app, pero la numeración, control de flujo y envío se realiza en el nivel de enlace

- Definir tamaño ventana
- Cantidad de tramas recibidas antes de enviar ACK
- Número de secuencia
- Se tiene que elegir entre rechazo simple y rechazo selectivo

## entradas de la simulación

- cantidad de tramas a enviar desde A
- Cantidad de tramas a enviar desde B
- Tamaño de las ventanas
- Número de tramas a enviar/recibir antes de recibir un ACK
- Probabilidad de error de envío








