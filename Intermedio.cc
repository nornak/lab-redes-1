/*
* Archivo que controla el nivel intermedio
*/

#include <string.h>
#include <omnetpp.h>

//Nombre de la clase y tipo
class Intermedio : public cSimpleModule
{
    //Métodos
    protected:
        //Procesador de mensajes desde la capa superior
        virtual void processMsgFromHigherLayer(cMessage *packet);
        //Procesador de mensajes desde la capa inferior
        virtual void processMsgFromLowerLayer(cMessage *packet);
        //Receptor de mensajes
        virtual void handleMessage(cMessage *msg);
};

//Definición de funciones
Define_Module( Intermedio );

//Manejador de mensajes
void Intermedio::handleMessage(cMessage *msg)
{
    //Si el mensaje viene desde abajo
    if (msg->arrivedOn("recibir_abajo"))
        //Enviar al procesador desde abajo
        processMsgFromLowerLayer(msg);
    //Sino, el mensaje viene desde arriba
    else
        //Procesar el mensaje desde arriba
        processMsgFromHigherLayer(msg);
}

//Procesador desde arriba
void Intermedio::processMsgFromHigherLayer(cMessage *packet)
{
    //Envia el paquete hacia la capa de enlace
    send(packet,"enviar_abajo");
}

//Procesador desde abajo
void Intermedio::processMsgFromLowerLayer(cMessage *packet)
{
    //Envía el paquete hacia la capa de aplicación
    send(packet,"enviar_arriba");
}
